# deploy

Terraform deployment for dependabot-gitlab

## components

Following terraform resources are managed in this repository:

- GKE cluster
- dependabot-gitlab helm chart
- Google managed certs
